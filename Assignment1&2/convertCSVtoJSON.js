let csv = require("convert-csv-to-json");
let inputFile = "./input.csv";
let outputFile = "./output.json";
let fs = require("fs");

let rawConvert = csv.fieldDelimiter(",").getJsonFromCsv(inputFile);

for (let index = 0; index < rawConvert.length; index++) {
  let birthday = new Date(rawConvert[index].date_of_birth);
  let first_name = rawConvert[index].first_name;
  let last_name = rawConvert[index].last_name;
  rawConvert[index].date_of_birth = changeFormDate(birthday);
  rawConvert[index].fullName = first_name + " " + last_name;
}

const data = JSON.stringify(rawConvert, null, 4);

fs.writeFileSync(outputFile, data);

function changeFormDate(birthday) {
  let date = birthday.getDate();
  let month = birthday.toLocaleString("default", { month: "short" });
  let year = birthday.getFullYear();
  return (birthday = `${date}-${month}-${year}`);
}
