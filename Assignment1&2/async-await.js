const wait = (s) => {
  return new Promise((resolve) => setTimeout(resolve, s * 1000));
};

async function test() {
  console.log("Start to wait!!");
  await wait(5);
  console.log("I waited here for 5 seconds");
}

test();
