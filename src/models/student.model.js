const mongoose = require('mongoose')
const mongoosePaginate = require('mongoose-paginate-v2')

const studentSchema = mongoose.Schema(
  {
    student_id: {
      type: String,
      trim: true,
    },
    name: {
      type: String,
      required: true,
      trim: true,
    },
    age: {
      type: Number,
      required: true,
      trim: true,
      min: 0,
    },
    address: {
      type: String,
      trim: true,
      required: true
    }
    ,
    course_id: {
      trim: true,
      type: String
    },
  }
)

studentSchema.plugin(mongoosePaginate)
const Student = mongoose.model("Student", studentSchema)

module.exports = Student
