const mongoose = require('mongoose')
const mongoosePaginate = require('mongoose-paginate-v2')

const teacherSchema = mongoose.Schema(
  {
    teacher_id: {
      type: String,
      trim: true,
    },
    name: {
      type: String,
      required: true,
      trim: true,
    },
    age: {
      type: Number,
      required: true,
      trim: true,
      min: 0,
    },
    address:{
      type: String,
      required: true,
      trim: true
    },
    course_id: {
      trim: true,
      type: []
    },
  }
)

teacherSchema.plugin(mongoosePaginate)
const Teacher = mongoose.model("Teacher", teacherSchema)

module.exports = Teacher
