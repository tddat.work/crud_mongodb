const mongoose = require('mongoose')

const courseSchema = mongoose.Schema(
  {
    course_id: {
      type: String,
      trim: true,
    },
    name: {
      type: String,
      required: true,
      trim: true,
    },
    teacher: {
      trim: true,
      required: true,
      type: String,
      default: 'teacher',
    },
  }
)
const Course = mongoose.model("Course", courseSchema)

module.exports = Course
