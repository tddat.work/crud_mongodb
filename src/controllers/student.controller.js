const { studentService } = require('../services')
const httpStatus = require('http-status');
const ApiError = require('../utils/ApiError');

const createStudent = async (req, res) => {
  const student = await studentService.createStudent(req.body);
  res.status(httpStatus.CREATED).send(student);
};

const deleteStudent = async (req, res) => {
  await studentService.deleteStudentByID(req.params.student_id)
  res.status(httpStatus.NO_CONTENT).send('true')
}

const getManyStudents = async (req, res) => {
  const {limit, page, sort, offset, ...filter} =  req.query
  let options = {
    limit: limit,
    page: page,
    sort: sort,
    offset: offset
  }
  Object.keys(options).forEach(key => {
    if(options[key] == undefined) {
      delete options[key]
    }
  })
  const students = await studentService.queryStudents(filter, options)
  res.send(students)
}

const getStudent = async (req, res) => {
  const student = await studentService.getStudentById(req.params.student_id)
  if (!student) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Student not found');
  }
  res.send(student);
}

const updateStudent = async (req, res) => {
  const student = await studentService.updateStudentById(req.params.student_id, req.body)
  console.log('ssssssssss', student)
  if( !student) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Student not found')
  }
  res.send('true')
}

module.exports = {
  createStudent,
  deleteStudent,
  getStudent,
  updateStudent,
  getManyStudents,
}