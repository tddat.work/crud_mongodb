module.exports.courseController = require('./course.controller')
module.exports.studentController = require('./student.controller')
module.exports.teacherController = require('./teacher.controller')