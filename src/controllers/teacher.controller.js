const { teacherService } = require('../services')
const httpStatus = require('http-status');
const ApiError = require('../utils/ApiError');

const createTeacher = async (req, res) => {
  const teacher = await teacherService.createTeacher(req.body);
  res.status(httpStatus.CREATED).send(teacher);
};

const deleteTeacher = async (req, res) => {
  await teacherService.deleteTeacherByID(req.params.teacher_id)
  res.status(httpStatus.NO_CONTENT).send('true')
}

const getManyTeachers = async (req, res) => {
  const {limit, page, sort, offset, ...filter} =  req.query
  let options = {
    limit: limit,
    page: page,
    sort: sort,
    offset: offset
  }
  Object.keys(options).forEach(key => {
    if(options[key] == undefined) {
      delete options[key]
    }
  })
  const teachers = await teacherService.queryTeachers(filter, options)
  res.send(teachers)
}

const getTeacher = async (req, res) => {
  const teacher = await teacherService.getTeacherById(req.params.teacher_id)
  if (!teacher) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Teacher not found');
  }
  res.send(teacher);
}

const updateTeacher = async (req, res) => {
  const teacher = await teacherService.updateTeacherById(req.params.teacher_id, req.body)
  console.log('ssssssssss', teacher)
  if( !teacher) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Teacher not found')
  }
  res.send('true')
}

module.exports = {
  createTeacher,
  deleteTeacher,
  getTeacher,
  updateTeacher,
  getManyTeachers,
}