const { courseService } = require('../services')
const httpStatus = require('http-status');

const createCourse = async (req, res) => {
  console.log("ssss", req.body)
  const course = await courseService.createCourse(req.body);
  res.status(httpStatus.CREATED).send(course);
};

const deleteCourse = async (req, res) => {
  await courseService.deleteCourseByID(req.params.course_id)
  res.status(httpStatus.NO_CONTENT).send()
}

const getCourse = async (req, res) => {
  const course = await courseService.getUserById(req.params.course_id)
  if (!course) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Course not found');
  }
  res.send(course);
}

module.exports = {
  createCourse,
  deleteCourse,
  getCourse,
}