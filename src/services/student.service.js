const httpStatus = require('http-status');
const {Student} = require('../models/index.model');
const ApiError = require('../utils/ApiError');

const createStudent = async (studentBody) => {
  const newStudent = await new Student(studentBody).save()
  return newStudent;
};


const getStudentById = async (id) => {
  return Student.findById(id);
};

const updateStudentById = async (student_Id, updateBody) => {
  const student = await getStudentById(student_Id);
  if (!student) {
    throw new ApiError(httpStatus.NOT_FOUND, 'User not found');
  }

  Object.assign(student, updateBody);
  await student.save();
  return student;
};

const deleteStudentByID = async (student_id) => {
  const student = await getStudentById(student_id)
  if( !student) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Student not found')
  }

  await student.remove()
  return student
}

const queryStudents = async (filter, options) => {
  const students = await Student.paginate(filter,options)
  return students
}

module.exports = {
  createStudent,
  deleteStudentByID,
  getStudentById,
  updateStudentById,
  queryStudents,
}