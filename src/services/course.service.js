const httpStatus = require('http-status');
const {Student} = require('../models/index.model');
const ApiError = require('../utils/ApiError');

const createStudent = async (studentBody) => {
  const newStudent = await new Student(studentBody).save()
  return newStudent;
};


const getUserById = async (id) => {
  return Student.findById(id);
};


const deleteStudentByID = async (student_id) => {
  const student = await getUserById(student_id)
  if( !student) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Student not found')
  }

  await student.remove()
  return student
}

module.exports = {
  createStudent,
  deleteStudentByID,
  getUserById,
}