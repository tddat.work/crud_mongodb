const httpStatus = require('http-status');
const {Teacher} = require('../models/index.model');
const ApiError = require('../utils/ApiError');

const createTeacher = async (teacherBody) => {
  const newTeacher = await new Teacher(teacherBody).save()
  return newTeacher;
};


const getTeacherById = async (id) => {
  return Teacher.findById(id);
};

const updateTeacherById = async (teacher_Id, updateBody) => {
  const teacher = await getTeacherById(teacher_Id);
  if (!teacher) {
    throw new ApiError(httpStatus.NOT_FOUND, 'User not found');
  }

  Object.assign(teacher, updateBody);
  await teacher.save();
  return teacher;
};

const deleteTeacherByID = async (teacher_id) => {
  const teacher = await getTeacherById(teacher_id)
  if( !teacher) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Teacher not found')
  }

  await teacher.remove()
  return teacher
}

const queryTeachers = async (filter, options) => {
  const teachers = await Teacher.paginate(filter,options)
  return teachers 
}

module.exports = {
  createTeacher,
  deleteTeacherByID,
  getTeacherById,
  updateTeacherById,
  queryTeachers,
}