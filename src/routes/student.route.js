const express = require('express');
const studentController = require('../controllers/student.controller')

const router = express.Router();

router
  .route('/')
  .post(studentController.createStudent)
  .get(studentController.getManyStudents)

router
  .route('/:student_id')
  .delete(studentController.deleteStudent)
  .get(studentController.getStudent)
  .put(studentController.updateStudent)

module.exports = router