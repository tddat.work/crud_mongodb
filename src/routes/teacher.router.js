const express = require('express');
const teacherController = require('../controllers/teacher.controller')

const router = express.Router();

router
  .route('/')
  .post(teacherController.createTeacher)
  .get(teacherController.getManyTeachers)

router
  .route('/:teacher_id')
  .delete(teacherController.deleteTeacher)
  .get(teacherController.getTeacher)
  .put(teacherController.updateTeacher)

module.exports = router