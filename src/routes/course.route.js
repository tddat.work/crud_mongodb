const express = require('express');
const { courseController } = require('../controllers');

const router = express.Router();

router
  .route('/')
  .post(courseController.createCourse)
  

router
  .route('/:course_id')
  .delete(courseController.deleteCourse)
  .get(courseController.getCourse)
  // .put(courseController.updateCourse)

module.exports = router